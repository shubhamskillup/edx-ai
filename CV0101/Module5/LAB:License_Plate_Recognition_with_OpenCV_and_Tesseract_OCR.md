<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# LAB: License Plate Recognition with OpenCV and Tesseract OCR (1h)


# LAB: License Plate Recognition with OpenCV and Tesseract OCR (External resource)

In this lab, you will learn to use OpenCV and Tesseract OCR to automatically recognize characters in license plates.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/CV0101EN/TesseractOpenCV.ipynb?lti=true) 


In case you encounter any issues opening the notebook using Skills Network Labs or want to view the notebook in your own Jupyter environment, you can download the Jupyter notebook (IPYNB file) by right-clicking on the link below and choosing "Save Link As...":

https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/CV0101/Labs/Module-5/Lab-TesseractOpenCV.ipynb
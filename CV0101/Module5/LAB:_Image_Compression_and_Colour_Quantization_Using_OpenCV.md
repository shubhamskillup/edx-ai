<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# LAB: Image Compression and Colour Quantization Using OpenCV (1h)

# LAB: Image Compression and Colour Quantization Using OpenCV (External resource)

In this lab, you will learn how to drastically reduce the size of images using K Means Clustering.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/CV0101EN/Image_Compression_using_OpenCV.ipynb?lti=true) 

In case you encounter any issues opening the notebook using Skills Network Labs or want to view the notebook in your own Jupyter environment, you can download the Jupyter notebook (IPYNB file) by right-clicking on the link below and choosing "Save Link As...":

https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/CV0101/Labs/Module-5/Lab-Image-Compression-and-Color-Quantization-using-OpenCV.ipynb
<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

# Task 3: Deploying your Web App

**Task 3: Deploying Your Web App**

The instructions for deploying your model as a Web App on the cloud are found in the Jupyter notebook in next section. Kindly launch the Jupyter notebook in the next section, and follow the step by step instructions and run the notebook with your Watson Visual Recognition credentials (email and password) to deploy your Web App on the cloud.

After deploying your Web App to the cloud, you will be able to access your Visual Recognition classifier as a webpage, that will look similar to the image below.

<img src="images/download3.png"/>

After Uploading your image in uploading area of the Web app, you will expect to get similar to the below result:

<img src="images/download4.png"/>


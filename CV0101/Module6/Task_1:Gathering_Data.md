<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# Task 1: Gathering Data

## Task 1: Gathering data

In order to train your Visual Recognition Classifier, you need to gather datasets of Fire, Smoke, and Neutral ( no Fire or Smoke) images. Collecting the right and diversified data is a very important component for building a good vision recognition model. To help you in finding the images, we are proposing the following sources and you can search for more sources. 

- [Google](https://www.google.com/search?q=fire+and+smoke+image+&tbm=isch&ved=2ahUKEwiXnMCUhoboAhXuVd8KHehaDmoQ2-cCegQIABAA&oq=fire+and+smoke+image+&gs_l=img.3..35i39j0i30.4889.4889..5453...0.0..0.69.69.1......0....1..gws-wiz-img.14wBn3EyZOU&ei=rFxiXpeWAu6r_QbotbnQBg&bih=757&biw=1656&hl=en)

- [Kaggle](https://www.kaggle.com/phylake1337/fire-dataset#fire.102.png)

- [Github](https://github.com/aiformankind/wildfire-smoke-detection/tree/master/input/images)

- [Unsplash](https://unsplash.com/s/photos/fire)

During your images collection process, we recommend you to: 

1. Collect **at least 160 images** for each set of labels (that is 160 images for Fire, 160 for Smoke and and 160 for Neutral)

2. Diverse the scene for each of your images dataset. For example:

- Different day time of a fire image in a forest during the morning, night and sunset.
- Smoke from a city, on Sea surface or from a Truck
- A building illuminated with red light as a neutral image

3. Keep around 5% (approx. 10 per label) of the collected images for testing the model

4. For the remainder 95% of the images (that is, about 150 per label) that will be used for training the model, create three different ".zip" files. Call them them fire.zip, smoke.zip, and neutral.zip respectively. This will make it easy for drag-and-drop into Watson Visual Recognition.
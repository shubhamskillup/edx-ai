<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

# Deploying Your Computer Vision Model on IBM Cloud

# Lab: Deploying Your Computer Vision Model on IBM Cloud (External resource)

For project deployment, you'll be running Jupyter notebook to deploy the customed classifier as a web app on IBM cloud a web app that allows users to upload images and classify them automatically with IBM Watson Visual Recognition.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/CV0101EN/Capstone-Project-Model-Deployment.ipynb?lti=true)


In case you encounter any issues opening the notebook using Skills Network Labs or want to view the notebook in your own Jupyter environment, you can download the Jupyter notebook (IPYNB file) by right-clicking on the link below and choosing "Save Link As..":

https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/CV0101/Project/Capstone-Project-Model-Deployment-edx.ipynb
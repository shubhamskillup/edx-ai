<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# Task 2: Training your Classifier

### Task 2: TRAINING YOUR CLASSIFIER

**Congratulations**, you are done with the toughest part, which is gathering the images dataset.  Now is the FUN part in which you will use Watson Visual Recognition to train your Classifier. In Module 4, you learned how to create a custom classifier using Watson Visual Recognition.

Please follow the steps outlined in a hands-on lab in module 4, but instead of using the dog Breeds dataset, in this exercise you will utilize the dataset of images labeled fire, smoke and neutral, to create the custom classifier.

For reference, here is the link to Watson Visual Recognition service on IBM Cloud:

[https://cocl.us/CV0101EN_IBM_Cloud_Registration](https://dataplatform.cloud.ibm.com/registration/stepone?cm_mmc=Email_Newsletter-_-Developer_Ed+Tech-_-WW_WW-_-Coursera-CV0101EN-cognitiveclass-skillsnetwork&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M12345678&cvosrc=email.Newsletter.M12345678&cvo_campaign=000026UJ)
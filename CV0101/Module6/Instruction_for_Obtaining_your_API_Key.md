<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

# Instruction for Obtaining your API Key

The following are the instructions to obtain the API Key for your Watson Visual Recognition API

First login to your IBM Cloud account.

https://cloud.ibm.com

Then under "services", click on your Watson Visual Recognition Service. Here the Watson Visual Recognition Service is called "watson-vision-combined-xx". The "Watson-vision-combined" is a fixed name but you can change "xx" part of it 

<img src="images/download1.png"/>

This will take you to your Watson Visual Recognition service, where you will find your API Key

<img src="images/download1.png"/>

Thank you.





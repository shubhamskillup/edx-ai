<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# Task 4: Testing Your Classifier

After you deploy your Visual Recognition classifier, anyone can classify images based on Fire, Smoke or Neutral ( no For or Smoke) using your classifier using the URL of your web app. All they have to do is enter the URL into any browser and upload an image. And they will get a classification of the image along with its class score.

In Task-1, you gathered 160 images for each label ( Fire, Smoke or Neutral) out of which 5 % ( approximately 10 images for each label) you kept for testing. You can use these testing images to test your classifier as shown below, before submitting your URL web app for grading.

<img src="Images/downloadcv1.png">


Your mark for this project will depend on how well your Web App correctly classifies an image based on Fire, Smoke or Neutral  (no Fire or Smoke).
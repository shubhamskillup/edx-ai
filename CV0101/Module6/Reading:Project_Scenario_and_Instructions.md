<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# Project Scenario and Tasks


In the previous modules, you learned how to train a custom Visual Recognition classifier. For the project in this module, you will develop a new custom classifier and learn how to deploy it as a web app on the cloud.

There are various advantages to deploying your Visual Recognition classifier to the cloud. First, you can access you can share your classifier and have anyone in the world, access it by entering the URL of your model into any web browser. Second, you can display your web app on your portfolio, and your potential future employers can interact with your project.

# PROJECT SCENARIO:

You are hired to work in the fire department of your city as an **AI Consultant**. The fire department is interested in developing a web app that can be fed remote/aerial surveillance images and used for detecting potential fires in remote locations in the city such as forests and neighboring villages. , This web app could serve as an early warning system that allows assisting the fire department to take swift action before the fires become widespread.

You will start by developing a Visual Recognition model that will classify any image as Fire, Smoke, or Neutral (i.e. no Fire or Smoke). You will utilize Watson Studio and the Watson Visual Recognition Service to develop this model and then deploy it as a web app by completing the tasks below.

**Task 1: Gathering the Dataset**

**Task 2: Training your Classifier**

**Task 3: Deploying your Web App**

**Task 4: Testing your Classifier**

**Task 5: Submit your Assignment and Evaluate your Peers** (for non-audit i.e. paid learners only)

Each of the above tasks will be described in detail in the sections that follow.
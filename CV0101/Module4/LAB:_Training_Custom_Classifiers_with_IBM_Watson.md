<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# LAB: Training Custom Classifiers with IBM Watson (1h)

# LAB: Training Custom Classifiers with IBM Watson (External resource)

In this lab, you will use Python to Train and Test your Visual Recognition Classifier.

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/CV0101EN/Training_Custom_Classifiers_with_Watson_v2.ipynb?lti=true) 


In case you encounter any issues opening the notebook using Skills Network Labs or want to view the notebook in your own Jupyter environment, you can download the Jupyter notebook (IPYNB file) by right-clicking on the link below and choosing "Save Link As...":


https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/CV0101/Labs/Module-4/Training_Custom_Classifiers_with_Watson.ipynb
<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# LAB: Classifying Images Using Watson VR and Python (1hr)

# LAB: Classifying Images Using Watson VR and Python (External resource)

In this lab, you will learn how to use Watson API to classify images with the Python programming language

[Start Lab](https://labs.cognitiveclass.ai/tools/jupyterlab/lab/tree/labs/CV0101EN/Classifying_Images_Using_Watson_API_v3.ipynb?lti=true)

In case you encounter any issues opening the notebook using Skills Network Labs or want to view the notebook in your own Jupyter environment, you can download the Jupyter notebook (IPYNB file) by right-clicking on the link below and choosing "Save Link As...":

https://s3.us.cloud-object-storage.appdomain.cloud/cf-courses-data/CognitiveClass/CV0101/Labs/Module-4/Classifying_Images_Using_Watson_API.ipynb
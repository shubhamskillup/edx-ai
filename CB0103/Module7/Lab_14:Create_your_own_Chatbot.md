<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# Create your own


## Exercise 1: (Optional) Create your own Chatbot

At this point in time, you have all the skills required to create a useful chatbot that understands the user. Your take-home assignment is to create your own chatbot. Whether a personal chatbot or something that can be useful for your company (internally or externally).

If you are truly out of ideas, consider the option of simply improving the chatbot we developed so far. There is lots of room for improvement and expansion (we haven't even used that @relationship entity we imported). But you have the tools to do so and that's what matters.

If you do end up creating a cool chatbot, please let us know either through a shout out on Twitter (I'm [@acangiano](https://twitter.com/acangiano) there) or by emailing me (cangiano@ca.ibm.com).

**We'd love to feature you and your work!**
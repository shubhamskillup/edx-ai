<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# Exercise 1: Familiarize yourself with the Analytics tab

While working on your dialog skill, you might have noticed an _Analytics_ tab.

<img src="Images/Capture12.PNG">

If you **click on it** , you'll see a dashboard with statistics and details about how your chatbot is
being used.

This is quite useful once you deploy your chatbot in the real world. You want to know how your
chatbot is being used, observe the conversation people are having with it (as shown in the
image below), and see if there are ways to improve the chatbot accordingly.

<img src="Images/Capture13.PNG">


For example, if you find out that a lot of people are asking about something that your chatbot
doesn't know how to handle, it might be time to create a new intent and node to handle that
scenario and provide helpful responses to the user.

Likewise, if Watson misclassified an intent, it would be good to correct it and you can do so
directly from the _Analytics_ > _User conversations_ tab.

<img src="Images/Capture14.PNG">

The statistics are also helpful because they tell you what your customers are focusing on. With
that knowledge in hand, you can invest more time to refine your chatbot to answer very in-
demand questions.

All this feedback can even be useful to refine the product itself at times.

For example, if the users complain to your chatbot that they are unable to find a certain
feature, it might be wise to improve the UI to make that feature more obvious or easy to find in
the app itself.

It's worth noting that this _Analytics_ tab will be empty for you if you haven't had some
conversation with the chatbot via the WordPress chat box. The reason for this is that _Try it
out_ sessions are not included in the _Analytics_ tab.

So, go ahead and **chat with your chatbot through the WordPress pop up chat window** , if you
haven't done so already. Then spend some time to **familiarize yourself with
the** **_Analytics_** **capabilities built in Watson Assistant** , by exploring this tab.

## Congratulations

Congratulations on completing all the labs within this course! With the knowledge acquired so
far, you should be able to build simple but useful chatbots and deploy them on websites
powered by WordPress.


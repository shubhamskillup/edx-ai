<img style="float:left;" src="Images/IDSNlogo.png" width="200" height="200"/>

# Lab 15: Become an IBM Partner


**Exercise 1 - Enroll in the IBM Partners program and build with Watson Assistant**

[Enroll today and receive IBM Cloud credits.](https://www.ibm.com/account/reg/in-en/signup?formid=urx-30677&src=CC_edX_CB0103EN_Freemium_AIBot250&cm_mmc=Email_External-_-Developer_Ed+Tech-_-WW_WW-_-Campaign-Cognitive+Class+CB0103EN-Courseware&cm_mmca1=000026UJ&cm_mmca2=10006555&cm_mmca3=M00000001&cvosrc=email.External.M00000001&cvo_campaign=000026UJ)

You'll get:

1. 100,000 API calls per month / 20 workspaces / 200 intents / 200 entities for a whole year;

2. 24/7 live chat to help answer your IBM Cloud architectural and technical evaluation questions;

3. The unique opportunity of being an IBM partner;

4. The credibility that comes from building solutions powered by IBM Watson.

**You don't need to provide a credit card to sign up. There are no strings attached, no autorenewals, or surprises.**

We want to enable you to make money without having you face any upfront costs or risks.

Note: **Offer only available to new IBM Cloud partners**. After the 1-year trial, you only pay for what you use on a pay as you go basis.

### For specific questions related to your IBM Partner application, please send IBM an email at isvsup@us.ibm.com.